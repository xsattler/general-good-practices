## Mocking an object and an invoked function
### CASE
```
class testClass:
    def foo(self):
        # Do something shady involving a db or a request
```

And somewhere in the code
```
def run_litigious_code():
    c = testClass()
    c.foo() 
```

### DON'T
```
@mock.patch('testClass')
def test_class(mock_class):
    run_litigious_code()

    mock_class.assert_called_once()
    mock_class.return_value.foo.assert_called_once()
```

### DO
```
@mock.patch('testClass')
@mock.patch('testClass.foo')
def test_class(mock_foo, mock_class):
    mock_class.return_value.foo = mock_foo
    
    run_litigious_code()

    mock_class.assert_called_once()
    mock_foo.assert_called_once()
```

### REASON
`mock_class.return_value.foo` assumes that testClass has a foo property / function.
If this function is removed from `testClass`, the test will not break. `@mock.patch('testClass.foo')`
raises an error in that case.