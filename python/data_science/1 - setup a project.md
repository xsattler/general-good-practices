# Setup a datascience project in Python

The goal of this article is to give setup instructions to new data scientists joining Ubees' team. I believe it can be useful for anyone willing to start a datascience project in python. Feel free to share, to provide feedbacks and even challenge my recommandations as many of them are purely based on personnal preferances. Who knows, I may even have missed something !



## Python : Python 3.7.4
When it comes to data science, two languages prevail : R and Python. At Ubees, we use exclusively Python.

Most of the time, it will be installed on your machine by default. Make sure however you are using Python 3 typing `python --version` in a terminal.
Support for Python 2.7 will end in january 2020, so **don't use python 2.7**. At Ubees, unless you have good reasons not to, use **`Python 3.7.4`**



## Python package manager : pip
In order to download a python package, use `pip`. The default command to do so is `pip install <PACKAGE>`.

In order to share your work with others, you need to keep track of the packages you installed. To do so, create a `requirements.txt` file. Each line of the file will be formatted as follow : `<PACKAGE>==<PACKAGE VERSION>`.

When you install a new package, use the command `pip freeze` to display all the installed packages. Look for the one you just installed, and copy past it in the `requirements.txt`.

If you want to install all packages in a `requirements.txt` file, use `pip install - requirements.txt`.

Some packages are only useful when you code, and not when you deploy your code (for example, `black`, the code formatter package - we will come back to it later). For those packages, create a `requirements_dev.txt` file, add `-r requirements.txt` as a first line. You can then add other dependencies, just like you would do for the `requirements.txt`.



## Virtual environment : virtualenv and virtualenvwrapper
You are about to create dozens of projects. You will be using a lot of different packages, and sometimes, as time flows, different versions of the same packages. Be aware that sometimes, a new version of a package is not compatible with the previous one, and may break your code.

In order to avoid this situation, use **virtual environments**. What it is is basically a new Python installation for each project, thus avoiding any conflict with the other ones.

To do so, use `pip install virtualenv`, and then `pip install virtualenvwrapper`.

You can now create a virtual environment with `mkvirtualenv --python=<YOUR PYTHON VERSION> <NAME OF THE ENVIRONMENT>`.

Exit the virtual environment with `deactivate`.

Go back in it with `workon <NAME OF THE ENVIRONMENT>`.



## IDE : use VSCode
Many people say Jupyter Notebook is a great place write code when you want to explore your data. I agree : being able to visualize your graphs just below your code is an amazing feature. However, I do not recommand using Jupyter Notebook or Jupyter Lab as it is far from being as rich and flexible as other IDE. I personnaly use Visual Studio Code.

It is possible to set VSCode up to behave like a notebook. You will be able to create cells with a way lighter syntaxe (just add a `# %%` comment to begin a cell and voilà !), making the code more portable and benefit from the flexibility and extendability of VSCode.

Tip : You can access VSCode settings with "Ctrl + ," and customize the shortcuts with "Ctrl + k + Ctrl + s".

In order to use the notebook display in VSCode, you need to install the `ipykernel` package, typing `pip install ipykernel` from within yout virtual environment.

In order to generate Jupyter Notebook files (nicely displayable in a web navigator), you can use [`Pweave`](http://mpastell.com/pweave/).


## VS Code extensions
There are two things that I love about VS Code.
1. There is a lot of extensions.
2. You can customize basically everthing, especially shortcuts.

Here is a list of the extensions I use:
* code-python-isort
* Coverage Gutters
* gitlab workflow
* GitLens
* Language-Cython
* Markdown Preview Enhanced
* Python
* Python for VSCode
* reStructuredText
* TODO Highlight

Here is a list of the shortcuts I find useful (some are custom):
* Got to beginning / end of the word `ctrl + arrow`
* Got to beginning / end of the line `fn + arrow`
* To move and select code, keet `shift` pressed
* Go to line  `ctrl + G`
* Delete line `ctrl + shift + k`
* Duplicate line above / below `ctrl (+ shift) + j`
* Comment / uncomment line `ctrl (+ shift) + :` (works with more than one line selected)
* Search in file `ctrl + F`
* Search in every file `ctrl + shift + F`
* Open file `ctrl + P`
* Focus editor `shift + alt + E`
* Focus terminal `shitf + alt + T`
* Split editor `shift + alt + !`
* Split terminal `shift + alt + n`
* Close editor `ctrl + w`
* Close terminal `shift + alt + w`
* Focus editor on the left / right `shift + alt + arrow`
* Focus terminal on the left / right `alt + arrow`


## Git and Gitlab
You will want to share your code with the rest of your team and to version your changes. To do so, using `git` is the best option. If you are not familiar with git, look for tutorials online, there are tons ! You need to be familiar with `commit`, `branch`, `checkout`, `push`, `pull` and `rebase`.

To host your code, use Github or Gitlab. I personally use Gitlab, but this is only a matter of personnal taste.
Using the `.gitlab-ci.yml` file, it is possible to set up continuous integration ( "CI" ) scripts, that will check everytime you share some code that it respects the teams coding conventions.

If there are files you don't want git to follow, use .gitignore (your `.vscode` configuration folder for example).



## Readme
Setup a README.md file right away. In this file, explain the basic information you know about your dataset and what you will try to find. When you are submerged with data, keeping your goal in mind is always useful.



## Clarify the team coding rules
Be aware that writing code is about three things : readability, ease of use and robustness.

**Readability** is about understanding what the code is about and how it works just by reading it, from a technical and from a business perspective.

**Ease of use** is about how long you need between the moment you discovered the code existence and you first use it **proprely**.

**Robustness** is about how confident you are the code does what it is supposed to do.

Thoses aspects are key, but nonetheless, when pressure is put on a team, they are the first to be sacrificed. Proper team rules help enforcing them and make sure you don't end up with an overwhelming technical debt.

### Readability
You will be many different contributors to your project. It will quickly become a mess if everyone writes code in its own way. Therefore, you want to make sure everyone follows the same conventions.

Make your life easier, instead of defining every rule by yourself and argue about good practices, use linters and code formatters. **Linters** are tools that check the code format. **Formatters** take it one step further, modifying your code to fit their taste.

In our case, we will be using `flake8` as a linter, `black` as a code formatter and `isort` as an import-only code formatter.

Those packages need to be in the `requirements_dev.txt`. As of today, `black` doesn't support `setup.cfg` file. `flake8` and `isort` do. What I did is write this file to have homogeneous rules. Here it is :
```
[flake8]
max-line-length = 88
select = C,E,F,W,B,B950
ignore = E501,W503,E203
exclude = venv

[isort]
line_length = 88
multi_line_output = 3
include_trailing_comma = True
skip_glob = venv
```

You can now start to argue about what those tools can't enforce.
I strongly suggest to follow the principles from `Clean Code` from Robert C. Martin.
Otherwise, respect `DRY`, `KISS` and `SRP`. (**Don't Repeat Yourself**, **Keep It Simple Stupid** and **Single Responsibility Principle**)

### Ease of use
Let's be clear : if your code is clean and readable, it will immediatly be easier to use than a messy one. It is not enough though, as no one wants to read code to understand what a function does. What you want to read is a proper english sentence telling you what it is all about.

This is why documentation matters. Even for simple or private projects.

In python, it is possible to write documentation inside the code, using `docstrings`, and use a tool like `Sphinx` to generate a html documentation. [Install it](http://www.sphinx-doc.org/en/master/usage/installation.html) globally to be able to use `sphinx-quickstart` command, and install it in your virtualenv with `pip install sphinx`.

At ubees, we follow [PEP257](https://www.python.org/dev/peps/pep-0257/). What it means is basically :
* Put a docstring in every module to say what its goal is.
* Provide complete explanation about
    * what the function does
    * what its arguments are
    * a simple example on how to use it

Docstring example:
```
def do_something(arg_1, arg_2):
    """String explaining what the function does. # the comment starts on the first line. No space before the first letter.
    
    Some more explanation can come here, after a blankline.
    
    Parameters
    ----------
    arg_1 : str
        Tell what this argument is
    arg_2 : int
        And this one too
    """
    return f"Do something with {arg_1} and {arg_2}"
```

From there, to run sphinx, you can simply use `python -m sphinx <source folder> <build folder>`.


### Robustness
Code is of no use if it is not run, and you don't want to run anything buggy (think about why we have `dev`, `int` and `prod` environments...) In order to make your code robust, you will have to implement tests. Those tests can be used when you code to check that what you implemented works properly, and afterward to make sure any other modification doesn't break the previously written code.

There are three types of tests. Unit tests, functionnal tests and integration tests.

* Unit tests : you just write a test to make sure your function does what it is supposed to. You have the right to use **lots** of mocking inside those tests, as you don't want to rely on any other function than the one tested.
* Functionnal test : you want to make sure that your functions have the proper behaviours. It means that you depend on all sub functions called by your function. Those tests are more robusts from a business perspective, but don't give as much info as unit tests about what doesn't work.
* Integration tests : when you have several applications/service calling each other, you want to make sure they all behave properly together (example : that the frontend and the backend have the same api, and not a different version). Those tests are the ultimate tests as they are the only one that matters in the end. They also are the trickiest to run.

You, as a team, need to decide what amount of time you will spend on what kind of tests. You need to choose a good library or framework to run those.

For unit test and functionnal tests, we use `pytest`.
TODO : integration tests

Also : remember that it is better to not test a function, but put a todo for later, rather than poorly testing it, making everyone believe you did the job when in fact, you haven't.


## Makefile
At some point, there will be a lot of commands to learn by heart (just to run `black`, `isort` and `flack8` in reformat or in check mode for example). You don't want to learn all of that.

You may want to create shortcuts of some sort. Creating bash aliases is an option. Using Makefiles is an other one.

I personnaly use make and Makefiles to solve this issue : I write my commands onces with the proper attributes, then enter `make` in my terminal followed by few `tabs` ans autocompletion leads me straight to the command I wanted.

I, for example, have a `make check-code-format` command that runs `black`, `isort` and `flack8` in line, to make sure all of my code is properly formatted before I do a git commit.



## Usefull libraries
Here is a list of useful libraries you may want to install and use during your project.
* Data manipulation
    * pandas
    * numpy
    * scipy
* Data visualization
    * matplotlib
    * seaborn
    * bokeh
    * plotly
    * pydot
* Machine learning
    * scikit-learn
    * statsmodel
    * XGBoost
    * LightGBM
    * CatBoost
    * Eli5

Don't got for deep learning libraries before it is absolutely needed
tensorflow
pyTorch
Keras

# TODO
Provide inks for every library / tool